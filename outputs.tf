output "distribution_id" {
  value = aws_cloudfront_distribution.cloudfront_distribution.id
}

output "domain_name" {
  value = var.domain_name
}

output "bucket_region" {
  value = aws_s3_bucket.s3_front_bucket.region
}

output "bucket_id" {
  value = aws_s3_bucket.s3_front_bucket.id
}

output "bucket_arn" {
  value = aws_s3_bucket.s3_front_bucket.arn
}

output "bucket_domain_name" {
  value = aws_s3_bucket.s3_front_bucket.bucket_domain_name
}
