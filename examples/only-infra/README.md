# Only infra example

This example show how to deploy all the AWS infra without uploading website content

## Requirements
- awccli
- terraform

## Export required env vars

### Solution 1 : Simple export
```bash
export AWS_ACCESS_KEY_ID="XXXXXXXXXXXXXXXXXXXX"
export AWS_SECRET_ACCESS_KEY="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
export AWS_DEFAULT_REGION="eu-west-1"
```

### Solution 2 : Export with direnv
Create a file named `.credentials` and put the following content
```bash
#!/bin/bash

export AWS_ACCESS_KEY_ID="XXXXXXXXXXXXXXXXXXXX"
export AWS_SECRET_ACCESS_KEY="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
export AWS_DEFAULT_REGION="eu-west-1"
```

## Update example.tfvars
Open the `example.tfvars` file and update the content with good values

## Run example
```bash
terraform init

terraform apply -var-file=example.tfvars
```
